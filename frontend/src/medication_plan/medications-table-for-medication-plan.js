import React from "react";
import Table from "../commons/tables/table";

const filters = [
    {
        accessor: 'name',
    }
];

const selectRow = {
    mode: 'checkbox',
    clickToSelect: true

};

class MedicationsForMedicationPlanTable extends React.Component {

    constructor(props) {
        super(props);
        const columns = [
            {
                Header: 'Id',
                accessor: 'id',
            },
            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: 'Side Effects',
                accessor: 'sideEffects',
            },
            {
                Header: 'Dosage',
                accessor: 'dosage',
            }];

        const sub_columns = columns.slice(0)
        sub_columns.push({
            id: 'button',
            accessor: 'id',

            Cell: val => (
                <input
                    type={"checkbox"}
                    onClick={() => this.props.handleCheck(val.original)}
                />
            )
        });

        this.state = {
            tableData: this.props.tableData,
            sub_columns: sub_columns,
        };
    }


    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.state.sub_columns}
                search={filters}
                pageSize={5}
            >
            </Table>
        )
    }

}

export default MedicationsForMedicationPlanTable;



