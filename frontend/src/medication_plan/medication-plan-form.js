import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_USERS from "../medication_plan/medication-plan-api";
import * as API_USERS_MED from "../medication/api/medication-api"
import validate from "../validators";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";

import {HOST} from "../commons/hosts";
import MedicationsForMedicationPlanTable from "./medications-table-for-medication-plan";

var medicationIdsSelected = [];

class MedicationPlanForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,
            tableData: [],
            formIsValid: false,

            formControls: {
                intakeInterval: {
                    value: '',
                    placeholder: 'intake interval...',
                    disabled: true,
                    check: true,
                    valid: true,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                treatmentPeriod: {
                    value: '',
                    placeholder: 'treatment period...',
                    valid: true,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCheck = this.handleCheck.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        console.log(name)
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    addMedicationPlan(medicationPlan) {
        return API_USERS.postMedicationPlan(medicationPlan, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully created medication plan with id: " + result);
                // this.reloadHandler();
                window.open(HOST.frontend_api + '/patient', '_self');

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    componentDidMount() {
        this.fetchMedications();
    }


    fetchMedications() {
        return API_USERS_MED.getMedications((result, status, err) => {
            console.log("result table");
            console.log(result);
            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleCheck(checkBox) {
        let medId = checkBox.id;
        if (medicationIdsSelected.includes(medId)) {
            //remove it
            medicationIdsSelected = medicationIdsSelected.filter((el) => el !== medId);
        } else {
            //add it
            medicationIdsSelected = medicationIdsSelected.concat(medId)
        }

    }


    handleSubmit() {
        let medicationPlan = {
            intakeInterval: this.state.formControls.intakeInterval.value,
            treatmentPeriod: this.state.formControls.treatmentPeriod.value,
            medicationIds: medicationIdsSelected,
            patientId: this.props.rowData.id
        };

        this.addMedicationPlan(medicationPlan);
    }

    render() {
        return (
            <div>

                <FormGroup id='intakeInterval'>
                    <Label for='intakeIntervalField'> Intake interval: </Label>
                    <Input name='intakeInterval' id='intakeIntervalField'
                           placeholder={this.state.formControls.intakeInterval.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.intakeInterval.value}
                           touched={this.state.formControls.intakeInterval.touched ? 1 : 0}
                           valid={this.state.formControls.intakeInterval.valid}
                           required
                    />
                    {this.state.formControls.intakeInterval.touched && !this.state.formControls.intakeInterval.valid &&
                    <div className={"error-message row"}> * Interval must have at least 3 characters </div>}
                </FormGroup>


                <FormGroup id='treatmentPeriod'>
                    <Label for='treatmentPeriodField'> Treatment period: </Label>
                    <Input name='treatmentPeriod' id='treatmentPeriodField'
                           placeholder={this.state.formControls.treatmentPeriod.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.treatmentPeriod.value}
                           touched={this.state.formControls.treatmentPeriod.touched ? 1 : 0}
                           valid={this.state.formControls.treatmentPeriod.valid}
                           required
                    />
                    {this.state.formControls.treatmentPeriod.touched && !this.state.formControls.treatmentPeriod.valid &&
                    <div className={"error-message row"}> * Treatment must have at least 3 characters </div>}
                </FormGroup>

                <Row>
                    <Col sm={{size: '8', offset: 1}}>
                        {this.state.isLoaded && <MedicationsForMedicationPlanTable
                            tableData={this.state.tableData}
                            handleCheck={this.handleCheck}
                        />}
                        {this.state.errorStatus > 0 && <APIResponseErrorMessage
                            errorStatus={this.state.errorStatus}
                            error={this.state.error}
                        />}
                    </Col>
                </Row>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        );
    }
}

export default MedicationPlanForm;
