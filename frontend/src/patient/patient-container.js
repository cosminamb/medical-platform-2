import * as React from "react";
import {Button, Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import * as API_USERS from "./api/patient-api"
import PatientTable from "./components/patient-table";
import PatientForm from "./components/patient-form";
import PatientUpdateForm from "./components/patient-form-update";
import MedicationPlanForm from "../medication_plan/medication-plan-form";


import {HOST} from "../commons/hosts";

class PatientContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.toggleAddModal = this.toggleAddModal.bind(this);
        this.toggleMedicationPlan = this.toggleMedicationPlan.bind(this);

        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            isOpenModalUpdate: false,
            isOpenModalMedicationPlan: false,

            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        if (localStorage.getItem('token') !== 'doctor') {
            window.open(HOST.frontend_api + '/login', '_self');
        } else {
            this.fetchPatients();
        }
    }


    fetchPatients() {
        return API_USERS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleAddModal(rowData) {
        this.setState({
                isOpenModalUpdate: !this.state.isOpenModalUpdate,
                rowData: rowData
            }
        );
    }

    toggleMedicationPlan(rowData) {
        this.setState({
                isOpenModalMedicationPlan: !this.state.isOpenModalMedicationPlan,
                rowData: rowData
            }
        );
    }


    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPatients();
    }


    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Patient Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Patient </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PatientTable
                                tableData={this.state.tableData}
                                toggleAddModal={this.toggleAddModal}
                                toggleMedicationPlan={this.toggleMedicationPlan}

                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add patient: </ModalHeader>
                    <ModalBody>
                        <PatientForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>


                <Modal isOpen={this.state.isOpenModalUpdate} toggle={this.toggleAddModal}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddModal}> Edit patient: </ModalHeader>
                    <ModalBody>
                        <PatientUpdateForm
                            reloadHandler={this.reload}
                            rowData={this.state.rowData}
                        />
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.isOpenModalMedicationPlan} toggle={this.toggleMedicationPlan}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleMedicationPlan}> Create medication plan: </ModalHeader>
                    <ModalBody>
                        <MedicationPlanForm
                            reloadHandler={this.reload}
                            rowData={this.state.rowData}
                        />
                    </ModalBody>
                </Modal>


            </div>
        )

    }

}


export default PatientContainer;