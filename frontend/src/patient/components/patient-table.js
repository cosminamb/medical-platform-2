import React from "react";
import Table from "../../commons/tables/table";
import Button from "@material-ui/core/Button";
import * as API_USERS from "../../patient/api/patient-api";
import {HOST} from "../../commons/hosts";


const filters = [
    {
        accessor: 'name',
    }
];

class PatientTable extends React.Component {

    constructor(props) {
        super(props);

        this.handleDelete = this.handleDelete.bind(this)
        this.openModalUpdate = this.openModalUpdate.bind(this)
        this.openModalMedicationPlan = this.openModalMedicationPlan.bind(this)
        const columns = [
            {
                Header: 'Id',
                accessor: 'id',
            },
            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: 'Address',
                accessor: 'address',
            },
            {
                Header: 'Age',
                accessor: 'age',
            },
            {
                Header: 'Birth Date',
                accessor: 'birthdate',
            },

            {
                Header: 'Gender',
                accessor: 'gender',
            },
            {
                Header: 'Medical Record',
                accessor: 'medicalRecord',
            },

        ];

        const sub_columns = columns.slice(0)
        sub_columns.push({
            id: 'button',
            accessor: 'id',
            Cell: ({value}) => (<Button onClick={() => {
                this.handleDelete(value);
            }}>Delete</Button>)
        });

        sub_columns.push({
            id: 'button',
            accessor: 'id',

            Cell: ({value}) => (<Button onClick={() => {
                this.openModalUpdate(value);
            }}>Update</Button>)
        });

        sub_columns.push({
            id: 'button',
            accessor: 'id',

            Cell: ({value}) => (<Button onClick={() => {
                this.openModalMedicationPlan(value);
            }}>Create medication plan</Button>)
        });

        this.state = {
            tableData: this.props.tableData,
            sub_columns: sub_columns,
            toggleAddModal: this.props.toggleAddModal,
            toggleMedicationPlan: this.props.toggleMedicationPlan,
        };
    }

    handleDelete = (id) => {
        console.log(id)
        return API_USERS.deletePatientById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted person with id: " + result);
                window.open(HOST.frontend_api + '/patient', '_self');

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    };

    openModalUpdate(value) {
        const rowData = this.findArrayElementById(this.state.tableData, value);
        this.state.toggleAddModal(rowData);
    }

    openModalMedicationPlan(value) {
        const rowData = this.findArrayElementById(this.state.tableData, value);
        this.state.toggleMedicationPlan(rowData);
    }

    findArrayElementById(array, id) {
        return array.find((element) => {
            return element.id === id;
        })
    }


    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.state.sub_columns}
                search={filters}
                pageSize={5}
            />
        )
    }
}

export default PatientTable;