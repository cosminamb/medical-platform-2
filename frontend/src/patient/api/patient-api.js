import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient'
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);

}


function getPatientById(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + '/' + id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function deletePatientById(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + '/' + id, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function updatePatientById(patient, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)


    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function postPatient(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    getPatients,
    getPatientById,
    postPatient,
    deletePatientById,
    updatePatientById,
};