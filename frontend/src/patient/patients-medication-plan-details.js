import React from 'react';
import {Card, Col, Row} from 'reactstrap';
import PatientMedicationTable from "./components/patients-medications-table";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";

class PatientsMedicationPlanCard extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);

        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: true,
            errorStatus: 0,
            error: null,
            medicationPlanId: null,
            patientDetails: null,
            intakeInterval: null,
            treatmentPeriod: null,

        };
        this.showMedicationPlan();

    }

    componentDidMount() {
    }

    showMedicationPlan() {
        let objPatDetails = this.props.patientDetails;
        if (objPatDetails.medicationPlanDTO !== null) {
            this.state.intakeInterval = objPatDetails.medicationPlanDTO.intakeInterval;
            this.state.treatmentPeriod = objPatDetails.medicationPlanDTO.treatmentPeriod;
            this.state.tableData = objPatDetails.medicationPlanDTO.medicationDTO;
        }
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
    }

    render() {
        return (
            <div>
                <Card>
                    <br/>
                    <p></p>
                    <p><strong> My medication plan: </strong></p>
                    <p> Interval: {this.state.intakeInterval} </p>
                    <p> Treatment Period: {this.state.treatmentPeriod} </p>
                    <br/>

                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PatientMedicationTable
                                tableData={this.state.tableData}
                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>
            </div>
        )

    }
}

export default PatientsMedicationPlanCard;
