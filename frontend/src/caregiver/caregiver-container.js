import * as React from "react";
import {Button, Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import * as API_USERS from "./api/caregiver-api"
import CaregiverTable from "./components/caregiver-table";
import CaregiverForm from "./components/caregiver-form";
import CaregiverUpdateForm from "./components/caregiver-form-update";
import {HOST} from "../commons/hosts";

class CaregiverContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.toggleAddModal = this.toggleAddModal.bind(this);

        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            isOpenModalUpdate: false,

            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        if (localStorage.getItem('token') !== 'doctor') {
            // this.props.history.push('/login')
            window.open(HOST.frontend_api + '/login', '_self')
        } else {
            this.fetchCaregivers();
        }
    }


    fetchCaregivers() {
        return API_USERS.getCaregivers((result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleAddModal(rowData) {
        this.setState({
                isOpenModalUpdate: !this.state.isOpenModalUpdate,
                rowData: rowData
            }
        );
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchCaregivers();
    }


    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Caregiver Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Caregiver </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <CaregiverTable
                                tableData={this.state.tableData}
                                toggleAddModal={this.toggleAddModal}

                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>


                <Modal isOpen={this.state.isOpenModalUpdate} toggle={this.toggleAddModal}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddModal}> Edit caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverUpdateForm
                            reloadHandler={this.reload}
                            rowData={this.state.rowData}
                        />
                    </ModalBody>
                </Modal>


            </div>
        )

    }

}


export default CaregiverContainer;