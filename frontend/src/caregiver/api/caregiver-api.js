import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    caregiver: '/caregiver'
};

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);

}


function getCaregiverById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/' + params, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function deleteCaregiverById(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/' + id, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function updateCaregiverById(caregiver, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)


    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function postCaregiver(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    getCaregivers,
    getCaregiverById,
    postCaregiver,
    deleteCaregiverById,
    updateCaregiverById,

};