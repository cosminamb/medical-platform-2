import React from 'react';
import {Card, CardHeader, Col, Row} from 'reactstrap';
import * as API_USERS from "./../caregiver/api/caregiver-api"
import CaregiversPatientsTable from "./components/caregivers-patients-table";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {HOST} from "../commons/hosts";

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';


class CaregiverMyPageContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            myName: null,
            myAge: null,
            myAddress: null,
            myBirthdate: null,
            myGender: null,
        };
    }

    componentDidMount() {
        this.fetchDetails();
        this.connect();
    }

    connect(){
        const webSocket = new SockJS(HOST.backend_api + '/activ');
        const clSocket = Stomp.over(webSocket);
        clSocket.connect({}, function (functionname) {
            console.log("Connected with " + functionname);
            clSocket.subscribe('/topic/messages', function (message) {
                console.log("message: ")
                console.log(message);
                window.alert(message.body);
            })
        })
    }

    fetchDetails() {
        let myobject = localStorage.getItem('details');
        let stringName = '';
        let stringAge = '';
        let stringAddress = '';
        let stringBirthdate = '';
        let stringGender = '';
        let stringMedicalRecord = '';
        let firstEnter = 1;
        JSON.parse(myobject, (key, val) => {
            switch (key) {
                case "name":
                    if (firstEnter) {
                        stringName = val;
                        firstEnter = 0;
                    }
                    break;
                case "age":
                    stringAge = val;
                    break;
                case "address":
                    stringAddress = val;
                    break;
                case "birthdate":
                    stringBirthdate = val.substring(0, 10);
                    break;
                case "gender":
                    stringGender = val;
                    break;
                case "medicalRecord":
                    stringMedicalRecord = val;
                    break;
                default:
                    break;
            }
        });

        this.state.myName = stringName;
        this.state.myAge = stringAge;
        this.state.myAddress = stringAddress;
        this.state.myBirthdate = stringBirthdate;
        this.state.myGender = stringGender;
        this.state.myMedicalRecord = stringMedicalRecord;

        JSON.parse(myobject, (key, val) => {
            if (key === "id") {
                return API_USERS.getCaregiverById(val, (result, status, err) => {
                    if (result !== null && status === 200) {
                        this.setState({
                            tableData: result.patientDTOList,
                            isLoaded: true
                        });
                    } else {
                        this.setState(({
                            errorStatus: status,
                            error: err
                        }));
                    }
                });
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    toggleAddModal(rowData) {
        this.setState({
                isOpenModalUpdate: !this.state.isOpenModalUpdate,
                rowData: rowData
            }
        );
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchDetails();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Management </strong>
                    <br/>
                    <br/>
                    <p> Name: {this.state.myName}</p>
                    <p> Age: {this.state.myAge}</p>
                    <p> Birth date: {this.state.myBirthdate}</p>
                    <p> Gender: {this.state.myGender}</p>
                    <p> Address: {this.state.myAddress}</p>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <CaregiversPatientsTable
                                tableData={this.state.tableData}
                                toggleAddModal={this.toggleAddModal}

                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                    <br/>
                </Card>


            </div>
        )

    }
}

export default CaregiverMyPageContainer;
