import React from "react";
import Table from "../../commons/tables/table";
import Button from "@material-ui/core/Button";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import PatientsMedicationPlanCard from "../../patient/patients-medication-plan-details";


const filters = [
    {
        accessor: 'name',
    }
];

class CaregiversPatientsTable extends React.Component {

    constructor(props) {
        super(props);

        this.openModalUpdate = this.openModalUpdate.bind(this);
        const columns = [
            {
                Header: 'Id',
                accessor: 'id',
            },
            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: 'Address',
                accessor: 'address',
            },
            {
                Header: 'Age',
                accessor: 'age',
            },
            {
                Header: 'Birth Date',
                accessor: 'birthdate',
            },

            {
                Header: 'Gender',
                accessor: 'gender',
            },
            {
                Header: 'Medical Record',
                accessor: 'medicalRecord',
            },
        ];

        const sub_columns = columns.slice(0)
        sub_columns.push({
            id: 'button',
            accessor: 'id',

            Cell: ({value}) => (<Button onClick={() => {
                this.openModalUpdate(value);
            }}>See medication plan</Button>)
        });

        this.state = {
            tableData: this.props.tableData,
            sub_columns: sub_columns,
            toggleAddModal: false,
            patientDetails: null,
        };
    }

    openModalUpdate(value) {
        const rowData = this.findArrayElementById(this.state.tableData, value);
        console.log(rowData);
        console.log("dsadf");

        this.setState({toggleAddModal: !this.state.toggleAddModal});
        this.state.patientDetails = rowData;
    }

    findArrayElementById(array, id) {
        return array.find((element) => {
            return element.id === id;
        })
    }


    render() {
        return (
            <div>
                <Table
                    data={this.state.tableData}
                    columns={this.state.sub_columns}
                    search={filters}
                    pageSize={5}
                />

                <Modal isOpen={this.state.toggleAddModal} toggle={this.openModalUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.openModalUpdate}> Patient's medication table: </ModalHeader>
                    <ModalBody>

                        <PatientsMedicationPlanCard patientDetails={this.state.patientDetails}/>

                    </ModalBody>
                </Modal>
            </div>

        )
    }
}

export default CaregiversPatientsTable;