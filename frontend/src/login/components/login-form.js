import * as React from "react";
import validate from "../../person/components/validators/person-validators";
import * as API_USERS from "../api/login-api";
import {redirect} from "../api/login-api";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card} from "react-bootstrap";

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            formControls: {
                username: {
                    value: '',
                    placeholder: 'What is your username?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                password: {
                    value: '',
                    placeholder: 'Password...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    login(user) {
        return API_USERS.loginApi(user, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully logged in user with object: " + result);
                localStorage.setItem('token', result.accTy);
                localStorage.setItem('details', JSON.stringify(result.person));
                redirect(result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        console.log(name)
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    handleSubmit() {
        let user = {
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value
        };
        this.login(user);
    }

    render() {
        return (
            <div align="center">

                <Card style={{width: '30rem', alignItems: "center", upperWidth: '50rem'}}>
                    <FormGroup id='username'>
                        <Label for='usernameField'> Username: </Label>
                        <Input name='username' id='usernameField'
                               placeholder={this.state.formControls.username.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.username.value}
                               touched={this.state.formControls.username.touched ? 1 : 0}
                               valid={this.state.formControls.username.valid}
                               required
                        />
                        {this.state.formControls.username.touched && !this.state.formControls.username.valid &&
                        <div className={"error-message row"}> * Username must have at least 3 characters </div>}
                    </FormGroup>

                    <FormGroup id='password'>
                        <Label for='passwordField'> Password: </Label>
                        <Input name='password' id='passwordField'
                               placeholder={this.state.formControls.password.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.password.value}
                               touched={this.state.formControls.password.touched ? 1 : 0}
                               valid={this.state.formControls.password.valid}
                               type={"password"}
                               required
                        />
                        {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
                        <div className={"error-message"}> * Password must not be null</div>}
                    </FormGroup>


                    <Row>
                        <Col sm={{size: '4', offset: 8}}>
                            <Button type={"submit"} disabled={!this.state.formIsValid}
                                    onClick={this.handleSubmit}> Submit </Button>
                        </Col>
                    </Row>

                    {
                        this.state.errorStatus > 0 &&
                        <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                    }
                </Card>
            </div>
        );
    }

}

export default LoginForm;