import {HOST} from './commons/hosts';
import {endpoint} from "./login/api/login-api";

import React from 'react'
import logo from './commons/images/icon.png';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';
import Button from "react-bootstrap/Button";

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBar = () => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand  href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
            </NavbarBrand>

            <Button className={"ml-auto"} color="primary" onClick={() => window.open(HOST.frontend_api+ endpoint.login, '_self')}>Login</Button>
            <p> ' ' </p>
            <Button  color="primary" onClick={() => { window.open(HOST.frontend_api+ endpoint.login, '_self'); localStorage.clear()}}>Logout</Button>
        </Navbar>
    </div>
);

export default NavigationBar
