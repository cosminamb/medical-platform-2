import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import PersonContainer from './person/person-container'
import LoginContainer from './login/login-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import PatientContainer from "./patient/patient-container";
import DoctorContainer from "./doctor/doctor-container";
import MedicationContainer from "./medication/medication-container";
import CaregiverContainer from "./caregiver/caregiver-container";
import DoctorMyPageContainer from "./doctor/doctor-my-page-container";
import CaregiverMyPageContainer from "./caregiver/caregiver-my-page-container";
import PatientMyPageContainer from "./patient/patient-my-page-container";

class App extends React.Component {

    // constructor(props) {
    //     super(props);
    //     // this.toggleForm = this.toggleForm.bind(this);
    //     // this.reload = this.reload.bind(this);
    //     this.state = {
    //         accountDetails:null,
    //         // selected: false,
    //         // collapseForm: false,
    //         // tableData: [],
    //         // isLoaded: false,
    //         // errorStatus: 0,
    //         // error: null
    //     };
    // }

    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/person'
                            render={() => <PersonContainer/>}
                        />

                        <Route
                            exact
                            path='/patient'
                            render={() => <PatientContainer/>}
                        />

                        <Route
                            exact
                            path='/doctor'
                            render={() => <DoctorContainer/>}
                        />

                        <Route
                            exact
                            path='/doctor/myPage'
                            render={() => <DoctorMyPageContainer/>}
                        />

                        <Route
                            exact
                            path='/medication'
                            render={() => <MedicationContainer/>}
                        />
                        <Route
                            exact
                            path='/caregiver'
                            render={() => <CaregiverContainer/>}
                        />

                        <Route
                            exact
                            path='/caregiver/myPage'
                            render={() => <CaregiverMyPageContainer />}
                        />

                        <Route
                            exact
                            path='/patient/myPage'
                            render={() => <PatientMyPageContainer />}
                        />


                        <Route
                            exact
                            path='/login'
                            render={() => <LoginContainer/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
