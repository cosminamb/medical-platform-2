import React from "react";
import Table from "../../commons/tables/table";
import * as API_USERS from "../api/medication-api";
import Button from "@material-ui/core/Button";
import {HOST} from "../../commons/hosts";


const filters = [
    {
        accessor: 'name',
    }
];

class MedicationTable extends React.Component {

    constructor(props) {
        super(props);

        this.handleDelete = this.handleDelete.bind(this)
        this.openModalUpdate = this.openModalUpdate.bind(this)
        const columns = [
            {
                Header: 'Id',
                accessor: 'id',
            },
            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: 'Side Effects',
                accessor: 'sideEffects',
            },
            {
                Header: 'Dosage',
                accessor: 'dosage',
            }];

        const sub_columns = columns.slice(0)
        sub_columns.push({
            id: 'button',
            accessor: 'id',
            Cell: ({value}) => (<Button onClick={() => {
                this.handleDelete(value);
            }}>Delete</Button>)
        });

        sub_columns.push({
            id: 'button',
            accessor: 'id',

            Cell: ({value}) => (<Button onClick={() => {
                this.openModalUpdate(value);
            }}>Update</Button>)
        });
        this.state = {
            tableData: this.props.tableData,
            sub_columns: sub_columns,
            toggleAddModal: this.props.toggleAddModal
        };
    }

    handleDelete = (id) => {
        console.log(id)
        return API_USERS.deleteMedicationById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted person with id: " + result);
                window.open(HOST.frontend_api + '/medication', '_self');
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    };

    openModalUpdate(value) {
        const rowData = this.findArrayElementById(this.state.tableData, value);
        this.state.toggleAddModal(rowData);
    }

    findArrayElementById(array, id) {
        return array.find((element) => {
            return element.id === id;
        })
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.state.sub_columns}
                search={filters}
                pageSize={5}
            >
            </Table>
        )
    }

}

export default MedicationTable;



