import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Container, Jumbotron} from 'reactstrap';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class Home extends React.Component {


    render() {

        return (

            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Welcome to our Integrated Medical Monitoring Platform </h1>
                        <br/>
                        <p className="lead" style={textStyle}>
                            <b>Home-care assistance is our job! We are happy to see you here! </b> <br/>
                            <b> We created this platform for enabling real time monitoring of patients, remote-assisted care services and
                                smart intake mechanism for prescribed medication.</b> <br/>
                            <b> Please login to benefit from all our services</b>
                        </p>
                        <hr className="my-2"/>
                        <br/>
                        <p  style={textStyle}> <b>For any concerns or urgent problems please contact us at (+44) 101 1001 </b> </p>
                        <p className="lead">
                        </p>
                    </Container>
                </Jumbotron>

            </div>
        )
    };
}

export default Home
