import React from 'react';
import {Button, Card, CardHeader, Col, Row} from 'reactstrap';
import {HOST} from "../commons/hosts";


class DoctorMyPageContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
    }


    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary"
                                    onClick={() => window.open(HOST.frontend_api + '/patient', '_self')}> See
                                Patients </Button>
                        </Col>
                    </Row>
                    <br/>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary"
                                    onClick={() => window.open(HOST.frontend_api + '/caregiver', '_self')}> See
                                Caregivers </Button>
                        </Col>
                    </Row>
                    <br/>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary"
                                    onClick={() => window.open(HOST.frontend_api + '/medication', '_self')}> See
                                Medications </Button>
                        </Col>
                    </Row>
                    <br/>

                </Card>


            </div>
        )

    }
}


export default DoctorMyPageContainer;
