package ro.tuc.ds2020.consumer;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.ActivityDTO;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@NoArgsConstructor
public class AnomalousActivityCheck {


    public long getMinutesOfActivity(ActivityDTO activityDTO) {
        Date start = activityDTO.getStartTime();
        Date end = activityDTO.getEndTime();
        long startTime = start.getTime();
        long endTime = end.getTime();
        long diff = endTime - startTime;
        return TimeUnit.MILLISECONDS.toMinutes(diff);
    }

    public boolean hasAnomalousSleepingActivity(ActivityDTO activityDTO) {
        long minutes = getMinutesOfActivity(activityDTO);
        if (minutes > 7 * 60) {
            return true;
        }
        return false;
    }

    public boolean hasAnomalousLeavingActivity(ActivityDTO activityDTO) {
        long minutes = getMinutesOfActivity(activityDTO);
        if (minutes > 5 * 60) {
            return true;
        }
        return false;
    }

    public boolean hasAnomalousBathroomActivity(ActivityDTO activityDTO) {
        long minutes = getMinutesOfActivity(activityDTO);
        if (minutes > 30) {
            return true;
        }
        return false;
    }

}
