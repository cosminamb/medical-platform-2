package ro.tuc.ds2020.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.services.ActivityService;

import java.util.ArrayList;
import java.util.List;

@Component
//@Controller
public class MessageConsumer {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    private static int i = 0;
    private List<ActivityDTO> anomalousActivities = new ArrayList<>();

//    @Autowired
//    public MessageConsumer(ActivityService activityService) {
//        this.activityService = activityService;
//    }

    @RabbitListener(queues = Constants.ACTIVITY_QUEUE)
    public void consumeMessageFromQueue(ActivityDTO activityDTO) {
        i++;
        System.out.println(i + ": " + activityDTO);
        String activityName = activityDTO.getActivityName().toLowerCase();
        AnomalousActivityCheck anomalousActivityCheck = new AnomalousActivityCheck();

        if (activityName.equals("sleeping")) {
            if (anomalousActivityCheck.hasAnomalousSleepingActivity(activityDTO)) {
                System.out.println("ANORMAL: " + activityDTO);
                anomalousActivities.add(activityDTO);
                activityService.insert(activityDTO);
                simpMessagingTemplate.convertAndSend("/topic/messages", "Anomalous sleeping activity");
            }
        } else {
            if (activityName.equals("leaving")) {
                if (anomalousActivityCheck.hasAnomalousLeavingActivity(activityDTO)) {
                    System.out.println("ANORMAL: " + activityDTO);
                    anomalousActivities.add(activityDTO);
                    activityService.insert(activityDTO);
                    simpMessagingTemplate.convertAndSend("/topic/messages", "Anomalous leaving activity");
                }
            } else {
                if (activityName.equals("toileting") || activityName.equals("showering") || activityName.equals("grooming")) {
                    if (anomalousActivityCheck.hasAnomalousBathroomActivity(activityDTO)) {
                        System.out.println("ANORMAL: " + activityDTO);
                        anomalousActivities.add(activityDTO);
                        activityService.insert(activityDTO);
                        simpMessagingTemplate.convertAndSend("/topic/messages", "Anomalous bathroom activity: " + activityName);
                    }
                }
            }
        }
    }


//    @Scheduled(fixedDelay = 1000, initialDelay = 500)
//    public void sendMessageForPopup(){
//        if(!anomalousActivities.isEmpty()){
//            String popupMessage = new String("Anomalous activities detected: ");
//            for(ActivityDTO a: anomalousActivities){
//                if(a.getActivityName().equals("Sleeping")){
//                    popupMessage += "Anomalous sleeping activity" + "\n";
//                    break;
//                }
//                if(a.getActivityName().equals("Leaving")){
//                    popupMessage += "Anomalous leaving activity"+ "\n";
//                    break;
//                }
//                if(a.getActivityName().equals("Toileting") || a.getActivityName().equals("Showering") || a.getActivityName().equals("Grooming")){
//                    popupMessage += "Anomalous bathroom activity: " + a.getActivityName() + "\n";
//                    break;
//                }
//            }
//            simpMessagingTemplate.convertAndSend("/topic/messages", popupMessage);
//        }
//    }

}
