package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDetailsDTO;
import ro.tuc.ds2020.dtos.MedicationPlanIdDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationRepository medicationRepository;
    private final PatientRepository patientRepository;


    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository, MedicationRepository medicationRepository, PatientRepository patientRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicationRepository = medicationRepository;
        this.patientRepository = patientRepository;
    }

    public List<MedicationPlanDTO> findMedicationPlans() {
        List<MedicationPlan> medicationPlanList = medicationPlanRepository.findAll();
        return medicationPlanList.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    public MedicationPlanDetailsDTO findMedicationPlanById(UUID id) {

        Optional<MedicationPlan> medicationPlanOptional = medicationPlanRepository.findById(id);
        if (!medicationPlanOptional.isPresent()) {
            LOGGER.error("MedicationPlan with id {} was not found in db", id);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + id);
        }
        return MedicationPlanBuilder.toMedicationPlanDetailsDTO(medicationPlanOptional.get());

    }

    public UUID insert(MedicationPlanIdDTO medicationPlanIdDTO) {
        MedicationPlan medicationPlan = MedicationPlanBuilder.toEntityFromId(medicationPlanIdDTO);
        if (medicationPlanIdDTO.hasMedicationIds()) {
            for (UUID medicationId : medicationPlanIdDTO.getMedicationIds()) {
                Optional<Medication> medicationOptional = medicationRepository.findById(medicationId);
                if (!medicationOptional.isPresent()) {
                    LOGGER.error("Medication with id {} was not found in db", medicationId);
                    throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + medicationId);
                }
                Medication medication = medicationOptional.get();
                medicationPlan.getMedicationList().add(medication);
            }

        }
        medicationPlan = medicationPlanRepository.save(medicationPlan);
        LOGGER.debug("MedicationPlan with id {} was inserted in db", medicationPlan.getId());
        if (medicationPlanIdDTO.getPatientId() != null) {
            Optional<Patient> optionalPatient = patientRepository.findById(medicationPlanIdDTO.getPatientId());
            if (!optionalPatient.isPresent()) {
                LOGGER.error("Patient with id {} was not found in db", medicationPlanIdDTO.getPatientId());
                throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + medicationPlanIdDTO.getPatientId());
            }
            Patient fromDB = optionalPatient.get();
            fromDB.setMedicationPlan(medicationPlan);
            patientRepository.save(fromDB);
        }
        return medicationPlan.getId();
    }

}
