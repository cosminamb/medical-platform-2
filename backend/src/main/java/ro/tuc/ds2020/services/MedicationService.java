package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationDetailsDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDetailsDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final MedicationRepository medicationRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository, MedicationPlanRepository medicationPlanRepository) {
        this.medicationRepository = medicationRepository;
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public List<MedicationDTO> findMedications() {
        List<Medication> medicationList = medicationRepository.findAll();
        return medicationList.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public MedicationDetailsDTO findMedicationById(UUID id) {
        Optional<Medication> prosumerOptional = medicationRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + id);
        }
        return MedicationBuilder.toMedicationDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(MedicationDetailsDTO medicationDTO) {
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        if (medicationDTO.hasMedicationPlanId()) {
            Optional<MedicationPlan> medicationPlanOptional = medicationPlanRepository.findById(medicationDTO.getMedicationPlanId());
            if (!medicationPlanOptional.isPresent()) {
                LOGGER.error("MedicationPlan with id {} was not found in db", medicationDTO.getMedicationPlanId());
                throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + medicationDTO.getMedicationPlanId());
            }
            MedicationPlan medicationPlan = medicationPlanOptional.get();//MedicationPlanBuilder.toEntity(medicationPlanDetailsDTO);
            medication.getMedicationPlans().add(medicationPlan);
        }
        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
        return medication.getId();
    }

    public UUID deleteMedicationById(UUID medicationId) {

        Optional<Medication> medicationOptional = medicationRepository.findById(medicationId);
        if (!medicationOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", medicationId);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + medicationId);
        }
        medicationRepository.deleteById(medicationId);

        LOGGER.debug("Medication with id {} was deleted ", medicationId);

        return medicationId;

    }

    public MedicationDetailsDTO updateMedication(MedicationDetailsDTO medicationDTO) {
        Medication fromDB = medicationRepository.findMedicationById(medicationDTO.getId()); //todo throw exception //findbyid simplu
        fromDB.setName(medicationDTO.getName());
        fromDB.setSideEffects(medicationDTO.getSideEffects());
        fromDB.setDosage(medicationDTO.getDosage());

        if (medicationDTO.getMedicationPlanId() != null) {
            Optional<MedicationPlan> medicationPlanOptional = medicationPlanRepository.findById(medicationDTO.getMedicationPlanId());
            if (!medicationPlanOptional.isPresent()) {
                LOGGER.error("MedicationPlan with id {} was not found in db", medicationDTO.getMedicationPlanId());
                throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + medicationDTO.getMedicationPlanId());
            }
            MedicationPlanDetailsDTO medicationPlanDetailsDTO = MedicationPlanBuilder.toMedicationPlanDetailsDTO(medicationPlanOptional.get());
            MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationPlanDetailsDTO);
            fromDB.getMedicationPlans().add(medicationPlan);

        }

        Medication medication = medicationRepository.save(fromDB);
        LOGGER.debug("Medication with id {} was updated ", medication.getId());

        return MedicationBuilder.toMedicationDetailsDTO(medication);
    }

}
