package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.dtos.builders.ActivityBuilder;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.ActivityRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class ActivityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);
    private ActivityRepository activityRepository;
    private PatientRepository patientRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository, PatientRepository patientRepository) {
        this.activityRepository = activityRepository;
        this.patientRepository = patientRepository;
    }


    public UUID insert(ActivityDTO activityDTO) {
        Activity activity = ActivityBuilder.toEntity(activityDTO);
        Optional<Patient> optionalPatient = patientRepository.findById(activityDTO.getPatientId());
        if(!optionalPatient.isPresent()){
            LOGGER.error("Patient with id {} was not found in db", activityDTO.getPatientId());
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + activityDTO.getPatientId());
        }
        Patient patient = optionalPatient.get();
        activity.setPatient(patient);
        activity = activityRepository.save(activity);
        LOGGER.debug("Activity with id {} was inserted in db", activity.getId());
        return activity.getId();
    }

}
