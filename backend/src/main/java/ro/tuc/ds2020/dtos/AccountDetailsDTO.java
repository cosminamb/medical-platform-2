package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class AccountDetailsDTO {

    private UUID id;
    @NotNull
    private String username;
    @NotNull
    private String password;
    private UUID accountTypeId;

    public AccountDetailsDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public AccountDetailsDTO(UUID id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public AccountDetailsDTO(String username, String password, UUID accountTypeId) {
        this.username = username;
        this.password = password;
        this.accountTypeId = accountTypeId;
    }

    public AccountDetailsDTO(UUID id, String username, String password, UUID accountTypeId) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.accountTypeId = accountTypeId;
    }

    public boolean hasAccountTypeId() {
        return this.accountTypeId != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDetailsDTO accountDTO = (AccountDetailsDTO) o;
        return Objects.equals(username, accountDTO.username) &&
                Objects.equals(password, accountDTO.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }

}
