package ro.tuc.ds2020.dtos;

import lombok.*;

import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
@Data
@ToString
public class ActivityDTO {

    private UUID patientId;
    private Date startTime;
    private Date endTime;
    private String activityName;

    @Builder
    public ActivityDTO(UUID patientId, Date start, Date end, String activityName) {
        this.patientId = patientId;
        this.startTime = start;
        this.endTime = end;
        this.activityName = activityName;
    }
}
