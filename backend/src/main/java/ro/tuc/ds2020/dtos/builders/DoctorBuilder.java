package ro.tuc.ds2020.dtos.builders;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.dtos.DoctorDetailsDTO;
import ro.tuc.ds2020.entities.Doctor;

@NoArgsConstructor
public class DoctorBuilder {

    public static DoctorDTO toDoctorDTO(Doctor doctor) {
        return new DoctorDTO(doctor.getId(), doctor.getName(), doctor.getAge(), doctor.getBirthdate(), doctor.getAddress(), doctor.getGender());
    }

    public static DoctorDetailsDTO toDoctorDetailsDTO(Doctor doctor) {
        return new DoctorDetailsDTO(doctor.getId(), doctor.getName(), doctor.getAddress(), doctor.getAge(), doctor.getBirthdate(), doctor.getGender());
    }

    public static Doctor toEntity(DoctorDetailsDTO doctorDetailsDTO) {
        return new Doctor(doctorDetailsDTO.getName(),
                doctorDetailsDTO.getAddress(),
                doctorDetailsDTO.getAge(),
                doctorDetailsDTO.getBirthdate(),
                doctorDetailsDTO.getGender());
    }

}
