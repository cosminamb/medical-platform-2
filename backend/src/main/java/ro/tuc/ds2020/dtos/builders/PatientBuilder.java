package ro.tuc.ds2020.dtos.builders;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.entities.Patient;

@NoArgsConstructor
public class PatientBuilder {

    public static PatientDTO toPatientDTO(Patient patient) {
        return PatientDTO.builder()
                .id(patient.getId())
                .name(patient.getName())
                .age(patient.getAge())
                .birthdate(patient.getBirthdate())
                .address(patient.getAddress())
                .gender(patient.getGender())
                .medicalRecord(patient.getMedicalRecord())
                .medicationPlanDTO(patient.getMedicationPlan() != null ? MedicationPlanBuilder.toMedicationPlanDTO(patient.getMedicationPlan()) : null)
                .build();
    }


    public static PatientDetailsDTO toPatientDetailsDTO(Patient patient) {
        return PatientDetailsDTO.builder()
                .id(patient.getId())
                .caregiverId(patient.getCaregiver() != null ? patient.getCaregiver().getId() : null)
                .medicationPlanId(patient.getMedicationPlan() != null ? patient.getMedicationPlan().getId() : null)
                .name(patient.getName())
                .address(patient.getAddress())
                .gender(patient.getGender())
                .medicalRecord(patient.getMedicalRecord())
                .birthdate(patient.getBirthdate())
                .age(patient.getAge())
                .build();
    }

    public static Patient toEntity(PatientDetailsDTO patientDetailsDTO) {
        return new Patient(patientDetailsDTO.getId(),
                patientDetailsDTO.getName(),
                patientDetailsDTO.getAddress(),
                patientDetailsDTO.getAge(),
                patientDetailsDTO.getBirthdate(),
                patientDetailsDTO.getGender(),
                patientDetailsDTO.getMedicalRecord());
    }
}
