package ro.tuc.ds2020.dtos;

import lombok.*;
import ro.tuc.ds2020.dtos.validators.annotation.AgeLimit;
import ro.tuc.ds2020.dtos.validators.annotation.GenderType;

import javax.validation.constraints.NotNull;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder(builderMethodName = "buildPersonDetailsDto")
@EqualsAndHashCode
public class PersonDetailsDTO {

    //    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String address;
    @AgeLimit(limit = 18)
    private int age;
    @NotNull
    private Date birthdate;
    @NotNull
    @GenderType
    private char gender;

}
