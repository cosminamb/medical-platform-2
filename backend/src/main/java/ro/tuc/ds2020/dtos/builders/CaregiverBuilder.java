package ro.tuc.ds2020.dtos.builders;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class CaregiverBuilder {

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDTO(caregiver.getId(), caregiver.getName(), caregiver.getAge(), caregiver.getBirthdate(), caregiver.getAddress(), caregiver.getGender());
    }

    public static CaregiverDetailsDTO toCaregiverDetailsDTO(Caregiver caregiver) {
        List<PatientDTO> patientDTOS = new ArrayList<>();
        if (!caregiver.getPatients().isEmpty()) {
            for (Patient p : caregiver.getPatients()) {
                patientDTOS.add(PatientBuilder.toPatientDTO(p));
            }
        }

        return CaregiverDetailsDTO.builder()
                .id(caregiver.getId())
                .name(caregiver.getName())
                .address(caregiver.getAddress())
                .gender(caregiver.getGender())
                .birthdate(caregiver.getBirthdate())
                .age(caregiver.getAge())
                .patientDTOList(patientDTOS)
                .build();
    }

    public static Caregiver toEntity(CaregiverDetailsDTO CaregiverDetailsDTO) {
        return new Caregiver(CaregiverDetailsDTO.getName(),
                CaregiverDetailsDTO.getAddress(),
                CaregiverDetailsDTO.getAge(),
                CaregiverDetailsDTO.getBirthdate(),
                CaregiverDetailsDTO.getGender());
    }

}
