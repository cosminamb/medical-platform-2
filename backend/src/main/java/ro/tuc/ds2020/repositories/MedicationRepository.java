package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.Medication;

import java.util.List;
import java.util.UUID;

public interface MedicationRepository extends JpaRepository<Medication, UUID> {

    List<Medication> findByName(String name);

    Medication findMedicationById(UUID id);

    void deleteById(UUID medicationId);

    void deleteByName(String name);

    void deleteAll();

}
