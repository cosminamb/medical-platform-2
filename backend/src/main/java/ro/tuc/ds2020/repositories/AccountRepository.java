package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Account;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AccountRepository extends JpaRepository<Account, UUID> {

    List<Account> findByUsername(String username);

    Account findAccountById(UUID id);


    @Query(value = "select a " +
            "FROM Account  a " +
            "WHERE a.username = :username " +
            "AND a.password = :password")
    Optional<Account> findAccountByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    void deleteById(UUID id);

    void deleteAll();


}
