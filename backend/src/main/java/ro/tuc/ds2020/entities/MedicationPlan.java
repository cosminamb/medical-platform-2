package ro.tuc.ds2020.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class MedicationPlan implements Serializable {

    private static final long serialVersionUID = -4862802744174968395L;
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "uuid")
    private UUID id;

    @Column(name = "intakeInterval", nullable = false)
    private String intakeInterval;

    @Column(name = "treatmentPeriod", nullable = false)
    private String treatmentPeriod;

    @OneToOne(mappedBy = "medicationPlan")
    private Patient patient;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "medication_medicationPlan",
            joinColumns = {@JoinColumn(name = "medicationPlan_id")},
            inverseJoinColumns = {@JoinColumn(name = "medication_id")})
    List<Medication> medicationList = new ArrayList<>();

    public MedicationPlan(String intakeInterval, String treatmentPeriod, List<Medication> medicationList) {
        this.intakeInterval = intakeInterval;
        this.treatmentPeriod = treatmentPeriod;
        this.medicationList = medicationList;
    }

    public MedicationPlan(String intakeInterval, String treatmentPeriod) {
        this.intakeInterval = intakeInterval;
        this.treatmentPeriod = treatmentPeriod;
    }

    public MedicationPlan(UUID id, String intakeInterval, String treatmentPeriod) {
        this.id = id;
        this.intakeInterval = intakeInterval;
        this.treatmentPeriod = treatmentPeriod;
    }
}
