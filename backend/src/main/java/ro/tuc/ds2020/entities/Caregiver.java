package ro.tuc.ds2020.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Caregiver extends Person {

    private static final long serialVersionUID = -6449616533578901612L;
    @OneToMany(mappedBy = "caregiver", orphanRemoval = false, fetch = FetchType.EAGER, cascade = CascadeType.PERSIST) //orph false : deletes ONLY the relationship between patient and caregiver
            List<Patient> patients;

    public Caregiver(String name, String address, int age, Date birthdate, char gender, List<Patient> patients) {
        super(name, address, age, birthdate, gender);
        this.patients = patients;
    }

    public Caregiver(String name, String address, int age, Date birthdate, char gender) {
        super(name, address, age, birthdate, gender);
        this.patients = new ArrayList<>();
    }


}
