SET FOREIGN_KEY_CHECKS = 0;

DELETE FROM person where id !='';
DELETE FROM caregiver where id !='';
DELETE FROM patient where id !='';
DELETE FROM doctor where id !='';

SET FOREIGN_KEY_CHECKS = 1;