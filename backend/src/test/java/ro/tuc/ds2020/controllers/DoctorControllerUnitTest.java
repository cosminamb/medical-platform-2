package ro.tuc.ds2020.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.DoctorDetailsDTO;
import ro.tuc.ds2020.services.DoctorService;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DoctorControllerUnitTest extends Ds2020TestConfig {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DoctorService service;

//    @Test
//    public void insertDoctorTest() throws Exception {
//        ObjectMapper objectMapper = new ObjectMapper();
//        DoctorDetailsDTO doctorDTO = new DoctorDetailsDTO("John", "Somewhere Else street", 22, new Date(1998, 12, 06), 'M');
//
//        mockMvc.perform(post("/doctor")
//                .content(objectMapper.writeValueAsString(doctorDTO))
//                .contentType("application/json"))
//                .andExpect(status().isCreated());
//    }

    @Test
    public void insertDoctorTestFailsDueToAge() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        DoctorDetailsDTO doctorDTO = new DoctorDetailsDTO("John", "Somewhere Else street", 17, new Date(2003, 1, 17), 'M');

        mockMvc.perform(post("/doctor")
                .content(objectMapper.writeValueAsString(doctorDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void insertDoctorTestFailsDueToNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        DoctorDetailsDTO doctorDTO = new DoctorDetailsDTO("John", null, 17, new Date(2003, 1, 17), 'M');

        mockMvc.perform(post("/doctor")
                .content(objectMapper.writeValueAsString(doctorDTO))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

}