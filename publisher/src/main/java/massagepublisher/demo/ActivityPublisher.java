package massagepublisher.demo;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/activity")
public class ActivityPublisher {

    @Autowired
    private RabbitTemplate template;

    @PostMapping
    public String sendActivity(@RequestBody ActivityDTO activity) {
        template.convertAndSend(Constants.ACTIVITY_EXCHANGE, Constants.ACTIVITY_ROUTING_KEY, activity);
        return "Success";
    }

}
