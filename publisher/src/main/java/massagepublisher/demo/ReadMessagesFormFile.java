package massagepublisher.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@RestController
public class ReadMessagesFormFile {

    private final ActivityPublisher activityPublisher;

    @Autowired
    public ReadMessagesFormFile(ActivityPublisher activityPublisher) {
        this.activityPublisher = activityPublisher;
    }

    @GetMapping("/sendActivity")
    public void read() {
        String pattern = new String("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        File activityFile = new File(Constants.FILE);
        try {
            FileReader fstream = new FileReader(activityFile);
            BufferedReader in = new BufferedReader(fstream);
            String id = in.readLine();
            System.out.println("ID IS " + id);
            String line;
            while ((line = in.readLine()) != null) {
                String[] tokens = line.split("\t\t");
                Date start = simpleDateFormat.parse(tokens[0]);
                Date end = simpleDateFormat.parse(tokens[1]);
                String activity = tokens[2];
                ActivityDTO activityFromFile = new ActivityDTO(UUID.fromString(id), start, end, activity);
                activityPublisher.sendActivity(activityFromFile);
            }
            fstream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.err.println("Couldn't open file");
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Couldn't read line");
        } catch (ParseException e) {
            e.printStackTrace();
            System.err.println("Couldn't parse date");

        }

    }

}
