package massagepublisher.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class ActivityDTO {

    private UUID patientId;
    private Date startTime;
    private Date endTime;
    private String activityName;
}
