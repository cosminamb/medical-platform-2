package massagepublisher.demo;

public class Constants {

    public static final String ACTIVITY_QUEUE = "activity_queue";
    public static final String ACTIVITY_EXCHANGE = "activity_exchange";
    public static final String ACTIVITY_ROUTING_KEY = "activity_routing_key";
    public static final String FILE = "activity.txt";

}
